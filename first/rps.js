var choices = ["rock", "paper", "scissors"];
var buttons = document.querySelectorAll('.button');
var score = [0, 0];

buttons.forEach(buttons => {
  buttons.addEventListener('click', e => {
    const userInput = buttons.value
    console.log(userInput);
    playGame(userInput);
  })
})

async function playGame(userInput){
  await new Promise(t => setTimeout(t, 200));

  userInput = choices.indexOf(userInput.toLowerCase())
  var opponentInput = Math.floor(Math.random() * 3);

  showOpponentsInput(opponentInput);

  findWinner(userInput, opponentInput);
}

function showOpponentsInput(opponentInput){
  var choice = document.getElementById('choice');
  choice.innerText = "Opponents choice was " + choices[opponentInput] + "!";
}

function mod(a, b) {
  c = a % b
  return (c < 0) ? c + b : c
}

function findWinner(userInput, opponentInput) {
  var winner = document.getElementById('winner');
  var user = document.getElementById('user');
  var opponent = document.getElementById('opponent');

  if (userInput == opponentInput) {
    winner.innerText = "It's a tie!";
    document.getElementById('winner').style.color = "#323232"
  }
  else if (mod((userInput - opponentInput), choices.length) < choices.length / 2) {
    winner.innerText = "You won!";
    document.getElementById('winner').style.color = "#03c4a1"
    score[0]++;
    user.innerText = score[0];
  } else {
    winner.innerText = "Opponent won!"
    document.getElementById('winner').style.color = "#fa7f72"
    score[1]++;
    opponent.innerText = score[1];
  }
}
