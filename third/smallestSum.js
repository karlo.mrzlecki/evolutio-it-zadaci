function findAllSums(N, B, res = [], currentSum = B){
    if(currentSum == 0){
        return [res];
    }

    result = [];

    for(var element of N){
        if(element <= currentSum){
            result = result.concat(findAllSums(N, B, res.slice().concat(element), currentSum - element));
        }
    }
    return result;
}

function findShortest(array, B){
    minLen = B;
    for (var sum of array){
        if(sum.length < minLen){
            minLen = sum.length;
        }
    }
    return minLen;
}

var N = prompt("Insert array elements separated by ','");
N = N.split(",")
var B = prompt("Enter target sum");

var sums = findAllSums(N, B);

if (sums.length > 0){
    console.log(findShortest(sums, B));
}else{
    console.log(-1);
}